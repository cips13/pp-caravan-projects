package generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MainClass {
	
	public static final String NOMIN = "2";
	public static final String NOMAX = "10";
	public static final Integer NB_LEVELS = 22;
	public static final String TMIN = String.valueOf(NB_LEVELS);
	public static final String SEGM_COST = "1"; 
	public static final String X = "0.05";
	public static final String Y = "500";
	public static final String Z = "0.2";
	public static final int NB_NODES = (int) (Math.pow(2,NB_LEVELS)-1);
	public static final int NB_SEGMENTS = NB_NODES*2;
	
	
	public static void main(String[] args) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File("../Caravana/buna200.in")));
			writer.write(NOMIN+System.lineSeparator());
			writer.write(NOMAX+System.lineSeparator());
			writer.write(TMIN+System.lineSeparator());
			writer.write(X+System.lineSeparator());
			writer.write(Y+System.lineSeparator());
			writer.write(String.valueOf(NB_NODES)+System.lineSeparator());
			writer.write(String.valueOf(NB_SEGMENTS)+System.lineSeparator());
			
			for(int i=0;i<NB_NODES;i++){
				writer.write(String.valueOf(i) + " " + String.valueOf(2*i) + " " + String.valueOf(2*i + 1)+System.lineSeparator());
			}
			int limit = (int) (Math.pow(2,NB_LEVELS)-2);
			for(int i=0; i< NB_SEGMENTS;i++){
				if(i==0)
				{
					writer.write(String.valueOf(i) + " 0 1 "+SEGM_COST+ " "+  Z +System.lineSeparator());
					continue;
				}
				if(i<limit)
					writer.write(String.valueOf(i) + " " + String.valueOf((int)Math.floor(i/2)) + " " + String.valueOf(i+1) + " "+ SEGM_COST + " "+ Z+System.lineSeparator());
				else
					writer.write(String.valueOf(i) + " " + String.valueOf((int)Math.floor(i/2)) + " " + String.valueOf(-1) + " "+ SEGM_COST + " "+ Z+System.lineSeparator());
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}

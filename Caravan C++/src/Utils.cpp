#include "../include/Utils.h"

int Utils::getRandomNum(int maxValue)
{
    srand((unsigned)time(0));
    return rand()%maxValue;
}
/**
* Metoda care intoarce nr de decizii in care caravana nu se desparte din succesiunea curenta de mutari
*/
int Utils::getNbOfNonSplits(vector<ExpResult*> scenario)
{
    int nbNonSplits=0;
    for(ExpResult* move : scenario){
        if(move->getAllInOneDir())
            nbNonSplits++;
    }
    return nbNonSplits;
}
/**
* Metoda care calculeaza ce intarziere ar putea introduce oamenii peste NOMAX din scenariul curent
*/
double Utils::getPeopleImpact(vector<ExpResult* > scenario)
{
    double yCost=0;
    for(ExpResult* move: scenario){
        double nbAfterAdd = 1.5*move->getNbOfPeople();
        if((int)nbAfterAdd > MainClass::NOMAX){
            Segment* s;
            if(move->getIndiferent())
                s = MainClass::nodes[move->getNodeId()]->getLeft();
            else
                s= MainClass::segments[move->getSegmentId()];

            int difference = (int)nbAfterAdd - MainClass::NOMAX;
            yCost += MainClass::X * s->getCost() * difference;
        }
    }
    if(MainClass::DEBUG_INTERPRET)
        std::cout << "Cost (fct de oamenii adaugati): " << yCost << endl;

    return yCost;
}
/**
* Metoda care intoarce nr total de oameni peste NOMAX din scenariul curent
*/
int Utils::getNoPeopleAboveNomax(vector<ExpResult* > scenario)
{
    int difference = 0;
    for(ExpResult* move : scenario){
        double nbAfterAdd = 1.5*move->getNbOfPeople();
        if((int)nbAfterAdd > MainClass::NOMAX){
            difference += (int)nbAfterAdd - MainClass::NOMAX;
        }
    }
    if(MainClass::DEBUG_INTERPRET)
       std::cout << difference << "oameni peste NOMAX." << endl;
    return difference;
}
/**
* Metoda care intoarce nr de caravane sub NOMIN din scenariul curent
*/
int Utils::getNoCaravansBelowNomin(vector<ExpResult* > scenario)
{
    int result = 0;
    for(ExpResult* move : scenario){
        if(move->getNbOfPeople() < MainClass::NOMIN)
            result++;
    }
    if(MainClass::DEBUG_INTERPRET)
        std::cout << result << " caravane sun NOMIN. " << endl;

    return result;
}

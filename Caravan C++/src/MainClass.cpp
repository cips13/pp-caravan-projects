#include "../include/MainClass.h"


int MainClass::NOMIN;
int MainClass::NOMAX;
double MainClass::TMIN;
double MainClass::X;
double MainClass::Y;
double MainClass::remainingReserve;
int MainClass::nbNodes;
int MainClass::nbSegments;
unordered_map<int, Node*> MainClass::nodes;
unordered_map<int, Segment*> MainClass::segments;

vector<int> MainClass::activeNodeIds;
vector<int> MainClass::activeSegmentIds;

vector<vector<ExpResult*>> MainClass::resultSet;
vector<ScenarioAnRes*> MainClass::scenarioAnalisysResult;

int MainClass::startingNo;
int MainClass::endingNo = 0;
int MainClass::nbCaravans = 0;
bool MainClass::run = false;


void MainClass::runSimulation(){

    ifstream input("buna200.in");
    /*citirea din fisier*/
    if(input.is_open()){
        loadFile(&input);
        readNodeInfo(&input);
        readSegmentInfo(&input);
        input.close();
    }

    MainClass::startingNo = (NOMIN+NOMAX)/2;
    Caravan* startingCaravan = new Caravan(startingNo);
    nbCaravans++;
    nodes[0]->setCaravan(startingCaravan);
    activeNodeIds.push_back(0);
    int step = 0;

    while(nbCaravans > 0){
        //cout << "===================== STEP " << step << "=====================" <<endl;
        //cout << "NODURI: " << endl;
        moveToRoad();
        //cout << endl << "Press Enter for segments ........." << endl;
        //cin.ignore();
        //cout << "SEGMENTE: ";
        moveToHut();
        //cout << endl;
        //cout << "Press Enter for next step ........" << endl;
        //cin.ignore();
        step++;
    }
    cout << endingNo << " oameni au ajuns la final (" << (double)endingNo/(double)startingNo*100 << "%)" << endl;

}
int MainClass::interpretResultSet(){
    scenarioAnalisysResult.clear();
    for(unsigned int i=0;i<resultSet.size();i++){
        vector<ExpResult*> scenario = resultSet[i];
        double yCost=0;
        yCost += Simulation::getStormImpact(scenario);
        yCost += Utils::getPeopleImpact(scenario);
        if(MainClass::DEBUG_INTERPRET)
            cout << "Y cost pe mutare ar fi " << yCost << endl;

        int nonSplitNb = Utils::getNbOfNonSplits(scenario);
        int surplus = Utils::getNoPeopleAboveNomax(scenario);
        int subNomin = Utils::getNoCaravansBelowNomin(scenario);
        ScenarioAnRes* anRes = new ScenarioAnRes(i,nonSplitNb,remainingReserve-yCost,surplus,subNomin);
        scenarioAnalisysResult.push_back(anRes);
    }
    cout << " Remaining Y " << remainingReserve << endl;
    return analizeScenarios();

}
bool compareScenarios(ScenarioAnRes* scenario1, ScenarioAnRes* scenario2){
        if(scenario1->getRemainingY()>=0 && scenario2->getRemainingY()>=0){
            //daca au ace;lasi surplus aleg varianta in care oamenii raman uniti
            if(scenario1->getSurplus()==scenario2->getSurplus()){
                return scenario1->getNbOfNonSplits() > scenario2->getNbOfNonSplits();
            }
        }
        else if(scenario1->getRemainingY()<0 && scenario2->getRemainingY()>=0){
            return false;
        }
        else if(scenario1->getRemainingY()>=0 && scenario2->getRemainingY()<0){
            return true;
        }
        else if(scenario1->getRemainingY()<0 && scenario2->getRemainingY()<0){
            if(scenario1->getNbBelowNOMIN()==scenario2->getNbBelowNOMIN()){
                return scenario1->getNbOfNonSplits() > scenario2->getNbOfNonSplits();
            }
            else
                return scenario1->getNbBelowNOMIN() > scenario2->getNbBelowNOMIN();
        }
}
int MainClass::analizeScenarios(){

    sort(scenarioAnalisysResult.begin(),scenarioAnalisysResult.end(),compareScenarios);
    ScenarioAnRes* first = scenarioAnalisysResult.at(0);
    if(DEBUG_ANALIZE_SCENARIOS){
        for(ScenarioAnRes* scen : scenarioAnalisysResult){
            scen->toString();
            cout<<endl;
        }
    }
    if(first->getRemainingY()<0)
        run=true;

    return first->getScenarioId();

}
void MainClass::genResultSet(vector<int> nodeIds, vector<ExpResult*> rezIntermediare){
    if(nodeIds.empty()){
        resultSet.push_back(rezIntermediare);
        return;
    }

    int nodeId = nodeIds[0];
    nodeIds.erase(nodeIds.begin());
    Node* currentNode = nodes[nodeId];
    int leftId = currentNode->getLeft()->getId();
    int rightId = currentNode->getRight()->getId();
    Segment* leftSegment = segments[leftId];
    Segment* rightSegment = segments[rightId];

    if(leftSegment->getCost() == rightSegment->getCost()){
        vector<ExpResult*> rezInt1 = rezIntermediare;
        vector<ExpResult*> rezInt2 = rezIntermediare;

        //se duc toti pe una din cai
        ExpResult* case1 = new ExpResult(0,currentNode->getCaravan()->getSize(),true,nodeId,true);
        rezInt1.push_back(case1);
        vector<int> nodeIdsCopy1 = nodeIds;
        genResultSet(nodeIdsCopy1,rezInt1);

        //se impart pe jumatate
        int leftSide = currentNode->getCaravan()->getSize()/2;
        int rightSide = currentNode->getCaravan()->getSize()-leftSide;
        ExpResult* case2a = new ExpResult(leftId,leftSide,false,nodeId,false);
        ExpResult* case2b = new ExpResult(rightId,rightSide,false,nodeId,false);
        rezInt2.push_back(case2a); rezInt2.push_back(case2b);
        vector<int> nodeIdsCopy2 = nodeIds;
        genResultSet(nodeIdsCopy2,rezInt2);
    }
    //daca costurile sunt diferite am : totiStanga, totiDeapta, sau jumatate jumatate
    else{
        vector<ExpResult*> rezInt1 = rezIntermediare;
        vector<ExpResult*> rezInt2 = rezIntermediare;
        vector<ExpResult*> rezInt3 = rezIntermediare;

        //se duc toti spre stanga
        ExpResult* case1 = new ExpResult(leftId,currentNode->getCaravan()->getSize(),false,nodeId,true);
        rezInt1.push_back(case1);
        vector<int> nodeIdsCopy1 = nodeIds;
        genResultSet(nodeIdsCopy1,rezInt1);

        //se duc toti spre dreapta
        ExpResult* case2 = new ExpResult(rightId,currentNode->getCaravan()->getSize(),false,nodeId,true);
        rezInt2.push_back(case2);
        vector<int> nodeIdsCopy2 = nodeIds;
        genResultSet(nodeIdsCopy2,rezInt2);

        //se impart
        int leftSide = currentNode->getCaravan()->getSize()/2;
        int rightSide = currentNode->getCaravan()->getSize()-leftSide;
        ExpResult* case3a = new ExpResult(leftId,leftSide,false,nodeId,false);
        ExpResult* case3b = new ExpResult(rightId,rightSide,false,nodeId,false);
        rezInt3.push_back(case3a); rezInt3.push_back(case3b);
        vector<int> nodeIdsCopy3 = nodeIds;
        genResultSet(nodeIdsCopy3,rezInt3);
    }


}

void MainClass::generatePosibilities(){
    resultSet.clear();
    genResultSet(activeNodeIds, vector<ExpResult*>());
}

/**
* Metoda care muta caravanele din noduri pe drumuri.
* Aici se iau deciziile referitoare la impartire.
*/
void MainClass::moveToRoad(){
    generatePosibilities();
    int bestScenarioIndex = interpretResultSet();
    if(DEBUG_GENERATE){
        printGenerateResults();
    }
    cout << "Scenariul cu index " << bestScenarioIndex << "a fost ales si va fi pus in executie.";
    vector<ExpResult*> scenario = resultSet[bestScenarioIndex];

    bool skip=false;
    for(ExpResult* mutare : scenario){
        if(skip){
            skip=false;
            continue;
        }
        Node* currentNode = nodes[mutare->getNodeId()];
        Caravan* currentCaravan = currentNode->getCaravan();
        int currentCaravanSize = currentCaravan->getSize();
        currentNode->setCaravan(NULL);

        if(mutare->getIndiferent()){
            if(Utils::getRandomNum(2)==0){
                currentNode->getLeft()->setCaravan(currentCaravan);
                activeSegmentIds.push_back(currentNode->getLeft()->getId());
                cout << "Caravana(" << currentCaravan->getSize()<<")pers din nodul "
                     << currentNode->getId() << " a cotit la stanga. " << currentCaravan->getSize()
                     << " oameni pe segmentul " << currentNode->getLeft()->getId();
            }
            else{
                currentNode->getRight()->setCaravan(currentCaravan);
                activeSegmentIds.push_back(currentNode->getRight()->getId());
                cout << "Caravana(" << currentCaravan->getSize()<<")pers din nodul "
                     << currentNode->getId() << " a cotit la dreapta. " << currentCaravan->getSize()
                     << " oameni pe segmentul " << currentNode->getRight()->getId();
            }
        }
        else{
            if(mutare->getAllInOneDir()){
                Segment* currentSegment = segments[mutare->getSegmentId()];
                currentSegment->setCaravan(currentCaravan);
                activeSegmentIds.push_back(mutare->getSegmentId());
                cout << "Caravana(" << currentCaravan->getSize()<<")pers din nodul "
                     << currentNode->getId() << " a cotit. " << currentCaravan->getSize()
                     << " oameni pe segmentul " << currentSegment->getId();
            }
            else{
                skip=true;
                int nbLeft = currentCaravanSize/2;
                int nbRight = currentCaravanSize-nbLeft;

                currentNode->getLeft()->setCaravan(new Caravan(nbLeft));
                activeSegmentIds.push_back(currentNode->getLeft()->getId());

                currentNode->getRight()->setCaravan(new Caravan(nbRight));
                activeSegmentIds.push_back(currentNode->getRight()->getId());

                delete currentCaravan;

                nbCaravans+=1;

                cout << "Caravana din nodul " << currentNode->getId() << " s-a impartit "
                     << nbLeft << " oameni au cotit la stanga pe drumul " << currentNode->getLeft()->getId()
                     << nbRight << " iar " << nbRight << " oameni au cotit la dreapta pe drumul " << currentNode->getRight()->getId() << endl;

            }
        }
    }
    activeNodeIds.clear();
    for(vector<ExpResult*> result : resultSet){
        for(ExpResult* part : result){
            delete part;
        }
    }
    resultSet.clear();
    for(ScenarioAnRes* scenario : scenarioAnalisysResult){
        delete scenario;
    }
    scenarioAnalisysResult.clear();

}
/**
* Metoda care muta caravanele de pe drumuri in nodurile urmatoare
* Aici se adauga oameni, se simuleaza atacul hotilor , sau furtunile de nisip
*/
void MainClass::moveToHut(){
    for(vector<int>::iterator it = activeSegmentIds.begin(); it != activeSegmentIds.end() ;){
        int segmentId = *it;
        if(Simulation::attackByThieves(segmentId))
            it=activeSegmentIds.erase(it);
        else
        {
            Simulation::addPeople(segmentId);
            it++;
        }

    }
    Simulation::stormAttack();
    Simulation::argue();
    cout << "Remaining Y " << remainingReserve << endl;
    if(remainingReserve<0){
        cout << "GAME OVER!" << endl;
        nbCaravans=-1;
        return;
    }
    //aici argue si add people si atack by thieves si mai jos doar mutarea
    for(vector<int>::iterator it = activeSegmentIds.begin(); it != activeSegmentIds.end();){
        int segmentId = *it;
        cout << "ACTIVE SEGMENT ID " << segmentId << endl;
        it = activeSegmentIds.erase(it);
        Segment* currentSegment = segments[segmentId];

        Node* inNode = currentSegment->getIn();
        Node* outNode = currentSegment->getOut();
        Caravan* currentCaravan = currentSegment->getCaravan();
        currentSegment->setCaravan(NULL);

        //daca capatul segmentului este oaza B
        if(currentSegment->getOut()==NULL){
            endingNo+=currentCaravan->getSize();
            cout << currentCaravan->getSize()+ "oameni au ajuns la oaza B de pe segmentul "
                 << currentSegment->getId() << endl;
            nbCaravans--;
        }
        else{
            outNode->setCaravan(currentCaravan);
            activeNodeIds.push_back(outNode->getId());
            cout << "Caravana (" << currentCaravan->getSize() << ")pers plecata din nodul "
                 << inNode->getId() << " a luat-o pe segmentul " << segmentId << " si a ajuns in nodul "
                 << outNode->getId()<< endl;
        }

    }
}
/**
* Metoda care incarca variablilele de sistem intr-un fisier
*/
void MainClass::loadFile(ifstream * input){
    *input >> MainClass::NOMIN;
    *input >> MainClass::NOMAX;
    *input >> MainClass::TMIN;
    *input >> MainClass::X;
    *input >> MainClass::Y;
    *input >> MainClass::nbNodes;
    *input >> MainClass::nbSegments;

    MainClass::remainingReserve = MainClass::Y;

}
/**
* Metoda citeste nodurile din fisier si creaza asocierea nod - segment stang si drept.
* De asemenea toate nodurile si segmentele sunt introduse in nodes si segments cu id-ul lor drept cheie
*/
void MainClass::readNodeInfo(ifstream * input){
    string line;
    for(int i =0 ; i < MainClass::nbNodes ;i++){
        getline(*input,line);
        if(!line.empty()){
           // cout << "Linie " << line << endl;
            istringstream iss(line);
            if(iss){
                int nodeId,segm1Id,segm2Id;
                iss >> nodeId >> segm1Id >> segm2Id;
                Segment* segm1 = new Segment(segm1Id);
                Segment* segm2 = new Segment(segm2Id);
                Node* n = new Node(nodeId,segm1,segm2);

                nodes[nodeId] = n;
                segments[segm1Id] = segm1;
                segments[segm2Id] = segm2;

                // cout << "P " << nodeId << segm1Id << segm2Id << endl;

            }

        }
        else
            i--;


    }

}
/**
* Se citesc informatiile despre segmente si se actualizeaza referintele nodurilor asociate acestora.
* Daca nodul de out are id -1 inseamna ca segmentul este direct legat de B, caz in care nodul are valoarea null
*/
void MainClass::readSegmentInfo(ifstream* input){
    string line;
    for(int i=0; i < MainClass::nbSegments; i++){
        getline(*input,line);
        if(!line.empty()){
            istringstream iss(line);
            //cout << "Linie " << line << endl;
            if(iss){
                int segmentId,inId,outId;
                double cost,z;
                iss >> segmentId >> inId >> outId >> cost >> z;
                //cout << "P " << segmentId << inId << outId << cost << z << endl;

                Node* in = nodes[inId];
                Node* out = outId == -1 ? NULL : nodes[outId];

                Segment* segment = segments[segmentId];
                segment->setIn(in);
                segment->setOut(out);
                segment->setCost(cost);
                segment->setZ(z);

            }
        }
        else
            i--;
    }
}
/**
* Metoda de debugging care afiseaza rezultatele generate
*/
void MainClass::printGenerateResults(){
    if(DEBUG_GENERATE){
        unsigned int i=0;
        for(vector<ExpResult*> course : resultSet){
            cout << "-------------- CAZ " << i << "----------";
            cout << endl << endl;

            for(ExpResult* expRes : course){
                expRes->toString();
                cout << endl;
            }
            i++;
        }
    }
}


#include "../include/ScenarioAnRes.h"

ScenarioAnRes::ScenarioAnRes(int scenarioId, int nbOfNonSplits,double remainingY,int surplus,int nbBelowNOMIN)
{
    this->scenarioId = scenarioId;
    this->nbOfNonSplits = nbOfNonSplits;
    this->remainingY = remainingY;
    this->surplus = surplus;
    this->nbBelowNOMIN = nbBelowNOMIN;
}

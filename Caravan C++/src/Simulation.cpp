#include "../include/Simulation.h"

/**
* Metoda care simuleaza adaugarea oamenilor pe parcursul segmentelor
*/
void Simulation::addPeople(int segmentId)
{
    Segment* currentSegment = MainClass::segments[segmentId];
    Caravan* currentCaravan = currentSegment->getCaravan();
    double half = floor(currentCaravan->getSize()/2);
    int nbAdded = Utils::getRandomNum((int)half+1);
    currentCaravan->setSize(currentCaravan->getSize()+nbAdded);
    std::cout << nbAdded << " oameni s-au alaturat caravanei de pe segmentul " << segmentId << endl;

}
/**
*   Metoda care simuleaza intazierea datorata de problemele de organizare la depasirea lui NOMAX
*/
void Simulation::argue()
{
    for(unsigned int i=0; i<MainClass::activeSegmentIds.size();i++){
        Segment* segment = MainClass::segments[MainClass::activeSegmentIds[i]];
        if(segment->getCaravan()->getSize()>MainClass::NOMAX){
            int difference = segment->getCaravan()->getSize()-MainClass::NOMAX;
            MainClass::remainingReserve-= difference*segment->getZ();
            std::cout << difference + " oameni in plus au intarziat caravana cu " << difference*segment->getZ()
                      << " pe segmentul " << segment->getId() << endl;
        }
    }
}
/**
* Metoda care simuleaza atacul unei caravane de catre hoti. Caravanma este distrusa ca rezultat
*/
bool Simulation::attackByThieves(int segmentId)
{
    Segment* currentSegment = MainClass::segments[segmentId];
    Caravan* currentCaravan = currentSegment->getCaravan();
    if(currentCaravan->getSize() < MainClass::NOMIN){
        std::cout << "Caravana de pe segmentul " << segmentId << " a fost atacata de hoti. "
                  << currentCaravan->getSize() << " oameni s-au pierdut" << endl;
        MainClass::nbCaravans--;
        return true;
    }
}
/**
* Metoda care simuleaza furtunile de nisip
*/
void Simulation::stormAttack()
{
    int segmentIndex = Utils::getRandomNum(MainClass::nbSegments);
    Segment* s = MainClass::segments[segmentIndex];
    double delay = s->getCost()*s->getZ();
    if(s->getCaravan()!=NULL){
        std::cout << "Caravana de pe segmentul " << s->getId() << " a fost lovita de furtuna si intarziata cu " << delay << endl;
        MainClass::remainingReserve-=delay;
    }
    else{
        std::cout << "Furtuna a lovit pe segmentul " << s->getId() << " care era gol. " << endl;
    }
}
/**
* Metoda care calculeaza intarziere posibila cauzata de o furtuna
*/
double Simulation::getStormImpact(vector<ExpResult* > scenario)
{
    double stormProbability = Simulation::calculateStormProbability(scenario);
    if(MainClass::DEBUG_INTERPRET_DETAILS)
        std::cout << "Storm probability: " << stormProbability << endl;

    double meanTimeOnRoad = Simulation::calculateMeanScenarioCost(scenario);
    if(MainClass::DEBUG_INTERPRET_DETAILS)
        std::cout << "Mean Cost: " << meanTimeOnRoad << endl;

    double meanZ = Simulation::calculateMeanZ(scenario);
    if(MainClass::DEBUG_INTERPRET_DETAILS)
        std::cout << "Mean Z Delay: " << meanZ << endl;

    double meanTimeOnRoadIfStorm = meanTimeOnRoad + meanTimeOnRoad*meanZ;
    double predictedCostByStormOnly = meanTimeOnRoadIfStorm*stormProbability + meanTimeOnRoad*(1-stormProbability);
    if(MainClass::DEBUG_INTERPRET)
        std::cout << "Cost(fct de probabilitate) daca ar lovi furtuna " << predictedCostByStormOnly << " dif: " << (predictedCostByStormOnly-meanTimeOnRoad) << endl;

    //ce impact va avea asupra lui y
    return predictedCostByStormOnly-meanTimeOnRoad;
}
/**
* Metoda care calculeaza Z-ul mediu al segmentelor pe care urmeaza sa intre caravanele in scenariul curent
*/
double Simulation::calculateMeanZ(vector<ExpResult* > scenario)
{
    double totalZ=0;
    for(ExpResult* move : scenario){
        if(move->getIndiferent()){
            double tempMean = (MainClass::nodes[move->getNodeId()]->getLeft()->getZ() + MainClass::nodes[move->getNodeId()]->getRight()->getZ())/2;
            totalZ += tempMean;
        }
        else
            totalZ += MainClass::segments[move->getSegmentId()]->getZ();
    }
    return totalZ/scenario.size();
}

double Simulation::calculateMeanScenarioCost(vector<ExpResult*> scenario)
{
    double totalCost=0;
    for(ExpResult* move: scenario){
        if(move->getIndiferent())
            totalCost += MainClass::nodes[move->getNodeId()]->getLeft()->getCost();
        else
            totalCost += MainClass::segments[move->getSegmentId()]->getCost();
    }
    return totalCost/scenario.size();
}

double Simulation::calculateStormProbability(vector<ExpResult*> scenario)
{
    double nbActiveSegments = scenario.size();
    return nbActiveSegments/MainClass::nbSegments;
}

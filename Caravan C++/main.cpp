#include <iostream>

#include "include/MainClass.h"

using namespace std;

int main()
{
    if(MainClass::DEBUG_ANALIZE_SCENARIOS){
        cout << "Hello world!" << endl;
    }
    cin.ignore();

    MainClass simulation;
    simulation.runSimulation();
    return 0;
}

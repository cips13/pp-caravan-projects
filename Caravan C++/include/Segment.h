#ifndef SEGMENT_H
#define SEGMENT_H

#define NULL 0

class Node;
class Caravan;
class Segment
{
    public:
        Segment(int id);
        int getId() { return id; }
        void setId(int val) { id = val; }
        Node * getIn() { return in; }
        void setIn(Node * val) { in = val; }
        Node * getOut() { return out; }
        void setOut(Node * val) { out = val; }
        double getCost() { return cost; }
        void setCost(double val) { cost = val; }
        double getZ() { return z; }
        void setZ(double val) { z = val; }
        Caravan* getCaravan() { return caravan; }
        void setCaravan(Caravan* val) { caravan = val; }
    protected:
    private:
        int id;
        Node * in;
        Node * out;
        double cost;
        double z;
        Caravan* caravan;
};

#endif // SEGMENT_H

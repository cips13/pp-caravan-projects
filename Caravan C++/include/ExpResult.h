#ifndef EXPRESULT_H
#define EXPRESULT_H

#include <iostream>

class ExpResult
{
    public:
        ExpResult(int segmentId, int nbOfPeople, bool indiferent, int nodeId,bool allInOneDir);
        int getNodeId() { return nodeId; }
        void setNodeId(int val) { nodeId = val; }
        int getSegmentId() { return segmentId; }
        void setSegmentId(int val) { segmentId = val; }
        int getNbOfPeople() { return nbOfPeople; }
        void setNbOfPeople(int val) { nbOfPeople = val; }
        bool getIndiferent() { return indiferent; }
        void setIndiferent(bool val) { indiferent = val; }
        bool getAllInOneDir() { return allInOneDir; }
        void setAllInOneDir(bool val) { allInOneDir = val; }
        void toString(){
            if(indiferent){
                std::cout << nbOfPeople << " oameni se afla in nodul " << nodeId << ". Vor coti toti st/dr";
            }
            else{
                std::cout << nbOfPeople << " o vor lua pe segmentul " << segmentId;
            }
        }
    protected:
    private:
        int nodeId;
        int segmentId;
        int nbOfPeople;
        bool indiferent;
        bool allInOneDir;
};

#endif // EXPRESULT_H

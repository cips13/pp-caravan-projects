#ifndef MAINCLASS_H
#define MAINCLASS_H

using namespace std;

#include <iostream>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

#include "Node.h"
#include "Segment.h"
#include "ScenarioAnRes.h"
#include "ExpResult.h"
#include "Caravan.h"
#include "Simulation.h"

#define NULL 0

class Node;
class Segment;
class ScenarioAnRes;
class ExpResult;

class MainClass
{
    public:
        static const bool DEBUG_GENERATE = false;
        static const bool DEBUG_INTERPRET = false;
        static const bool DEBUG_INTERPRET_DETAILS = false;
        static const bool DEBUG_ANALIZE_SCENARIOS = false;

        static int NOMIN;
        static int NOMAX;
        static double TMIN;
        static double X;
        static double Y;

        static int nbNodes;
        static int nbSegments;

        static unordered_map<int, Node*> nodes;
        static unordered_map<int, Segment*> segments;

        static int startingNo;
        static int endingNo;

        static int nbCaravans;
        static double remainingReserve;

        static vector<int> activeNodeIds;
        static vector<int> activeSegmentIds;

        static vector<vector<ExpResult*>> resultSet;
        static vector<ScenarioAnRes*> scenarioAnalisysResult;

        static bool run;

        void runSimulation();
    protected:
    private:
        void loadFile(ifstream * input);
        void readNodeInfo(ifstream * input);
        void readSegmentInfo(ifstream * input);
        void moveToRoad();
        void generatePosibilities();
        void genResultSet(vector<int> nodeIds, vector<ExpResult*> rezIntermediare);
        int interpretResultSet();
        int analizeScenarios();
        void printGenerateResults();
        void moveToHut();
};

#endif // MAINCLASS_H

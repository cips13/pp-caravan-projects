#ifndef CARAVAN_H
#define CARAVAN_H


class Caravan
{
    public:
        Caravan(int size);
        int getSize(){ return size; }
        void setSize(int value){ size = value; }
    protected:
    private:
        int size;
};

#endif // CARAVAN_H

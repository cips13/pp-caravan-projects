#ifndef NODE_H
#define NODE_H

class Segment;
class Caravan;
class Node
{
    public:
        Node(int id, Segment* left, Segment* right);
        int getId() { return id; }
        void setId(int val) { id = val; }
        Segment* getLeft() { return left; }
        void setLeft(Segment* val) { left = val; }
        Segment* getRight() { return right; }
        void setRight(Segment* val) { right = val; }
        Caravan* getCaravan() { return caravan; }
        void setCaravan(Caravan* val) { caravan = val; }
    protected:
    private:
        int id;
        Segment* left;
        Segment* right;
        Caravan* caravan;
};

#endif // NODE_H

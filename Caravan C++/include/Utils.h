#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <cstdlib>
#include <ctime>
#include "ExpResult.h"
#include "MainClass.h"

using namespace std;
class Utils
{
    public:
        static int getRandomNum(int maxValue);
        static int getNbOfNonSplits(vector<ExpResult*> scenario);
        static double getPeopleImpact(vector<ExpResult*> scenario);
        static int getNoPeopleAboveNomax(vector<ExpResult*> scenario);
        static int getNoCaravansBelowNomin(vector<ExpResult*> scenario);
    protected:
    private:
};

#endif // UTILS_H

#ifndef SCENARIOANRES_H
#define SCENARIOANRES_H

#include <iostream>
class ScenarioAnRes
{
    public:
        ScenarioAnRes(int scenarioId, int nbOfNonSplits,double remainingY,int surplus,int nbBelowNOMIN);
        int getScenarioId() { return scenarioId; }
        void setScenarioId(int val) { scenarioId = val; }
        int getNbOfNonSplits() { return nbOfNonSplits; }
        void setNbOfNonSplits(int val) { nbOfNonSplits = val; }
        double getRemainingY() { return remainingY; }
        void setRemainingY(double val) { remainingY = val; }
        int getSurplus() { return surplus; }
        void setSurplus(int val) { surplus = val; }
        int getNbBelowNOMIN() { return nbBelowNOMIN; }
        void setNbBelowNOMIN(int val) { nbBelowNOMIN = val; }
        void toString(){
            std::cout << "ScenID " << scenarioId << " remY " << remainingY
                      << " nbOfNonSplits " << nbOfNonSplits << " surplus "
                      << surplus << " nbCar below NOMIN " << nbBelowNOMIN << std::endl;
        }
    protected:
    private:
        int scenarioId;
        int nbOfNonSplits;
        double remainingY;
        int surplus;
        int nbBelowNOMIN;
};

#endif // SCENARIOANRES_H

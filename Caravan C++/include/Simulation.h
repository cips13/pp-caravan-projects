#ifndef SIMULATION_H
#define SIMULATION_H

#include <math.h>

#include "ExpResult.h"
#include "MainClass.h"
#include "Utils.h"


class ExpResult;
class Simulation
{
    public:
        static void addPeople(int segmentId);
        static void argue();
        static bool attackByThieves(int segmentId);
        static void stormAttack();
        static double getStormImpact(vector<ExpResult*> scenario);
        static double calculateMeanZ(vector<ExpResult*> scenario);
        static double calculateMeanScenarioCost(vector<ExpResult*> scenario);
        static double calculateStormProbability(vector<ExpResult*> scenario);
    protected:
    private:
};

#endif // SIMULATION_H

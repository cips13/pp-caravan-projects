package profiling;

public class Segment 
{
	private Integer id;
	private Node in;
	private Node out;
	private Double cost;
	private Double z;
	private Caravan caravan;
	
	public Segment(Integer id)
	{
		this.setId(id);
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Node getIn()
	{
		return in;
	}

	public void setIn(Node in)
	{
		this.in = in;
	}

	public Node getOut()
	{
		return out;
	}

	public void setOut(Node out)
	{
		this.out = out;
	}

	public Double getCost()
	{
		return cost;
	}

	public void setCost(Double cost)
	{
		this.cost = cost;
	}

	public Caravan getCaravan()
	{
		return caravan;
	}

	public void setCaravan(Caravan caravan)
	{
		this.caravan = caravan;
	}

	public Double getZ()
	{
		return z;
	}

	public void setZ(Double z)
	{
		this.z = z;
	}
	

}

package profiling;

public class Caravan 
{
	private int size;
	
	public Caravan(int size)
	{
		this.size=size;
	}
	
	public void setSize(int size)
	{
		this.size = size;
	}
	public int getSize()
	{
		return size;
	}
	
}

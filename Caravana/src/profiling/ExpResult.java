package profiling;

public class ExpResult
{
	private Integer nodeId;
	private Integer segmentId;
	private Integer nbOfPeople;
	private boolean indiferent;
	private boolean allInOneDir;
	
	public ExpResult(Integer segmentId, Integer nbOfPeople,boolean indiferent,Integer nodeId, boolean allInOneDir)
	{
		this.segmentId = segmentId;
		this.nbOfPeople = nbOfPeople;
		this.indiferent = indiferent;
		this.nodeId = nodeId;
		this.allInOneDir = allInOneDir;
	}
	
	@Override
	public String toString()
	{
		String result;
		if(indiferent)
		{
			result = nbOfPeople + " oameni se afla in nodul "+nodeId+". Vor coti toti st/dr";
		}
		else
		{
			result = nbOfPeople + " o vor lua pe segmentul "+ segmentId;
		}
		return result;
	}
	public Integer getSegmentId()
	{
		return segmentId;
	}
	public Integer getNbOfPeople()
	{
		return nbOfPeople;
	}
	public void setSegmentId(Integer segmentId)
	{
		this.segmentId = segmentId;
	}
	public void setNbOfPeople(Integer nbOfPeople)
	{
		this.nbOfPeople = nbOfPeople;
	}

	public boolean isIndiferent()
	{
		return indiferent;
	}

	public void setIndiferent(boolean indiferent)
	{
		this.indiferent = indiferent;
	}

	public Integer getNodeId()
	{
		return nodeId;
	}

	public void setNodeId(Integer nodeId)
	{
		this.nodeId = nodeId;
	}

	public boolean isAllInOneDir()
	{
		return allInOneDir;
	}

	public void setAllInOneDir(boolean allInOneDir)
	{
		this.allInOneDir = allInOneDir;
	}
	
}

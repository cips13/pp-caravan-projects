package profiling;
import java.util.ArrayList;


public class Simulation
{
	/**
	 * Metoda care simuleaza adaugarea oamenilor pe parcursul segmentelor
	 */
	static void addPeople(Integer segmentId)
	{
		Segment currentSegment = MainClass.segments.get(segmentId);
		Caravan currentCaravan = currentSegment.getCaravan();
		Double half = Math.floor(currentCaravan.getSize()/2);
		Integer nbAdded = Utils.genRandomNum(half.intValue()+1);
		currentCaravan.setSize(currentCaravan.getSize()+nbAdded);
		System.out.println(nbAdded+" oameni s-au alaturat caravanei de pe segmentul "+segmentId);
	}
	
	/**
	 * Metoda care simuleaza intarzierea datorata de problemele de oraganizare la depasirea lui NOMAX
	 */
	static void argue()
	{
		for(int i = 0; i<MainClass.activeSegmentIds.size();i++)
		{
			Segment segment = MainClass.segments.get(MainClass.activeSegmentIds.get(i));
			if(segment.getCaravan().getSize()>MainClass.NOMAX)
			{
				int difference = segment.getCaravan().getSize()-MainClass.NOMAX;
				MainClass.remainingReserve=MainClass.remainingReserve - difference*segment.getZ();
				System.out.println(difference+" oameni in plus au intarziat caravana cu "+difference*segment.getZ()
						+" pe segmentul "+segment.getId());
			}
		}
	}
	/**
	 * Metoda care simuleaza atacul unei Caravane de catre hoti. Caravana este distrusa ca rezultat.
	 */
	static boolean attackByThieves(Integer segmentId)
	{
		Segment currentSegment = MainClass.segments.get(segmentId);
		Caravan currentCaravan = currentSegment.getCaravan();
		if(currentCaravan.getSize()<MainClass.NOMIN)
		{
			System.out.println("Caravana de pe segmentul "+ segmentId+" a fost atacata de hoti. "
							   +currentCaravan.getSize()+" oameni s-au pierdut");
			MainClass.nbCaravans--;
			return true;
		}
		return false;
	}
	/**
	 * Metoda care simuleaza furtunile de nisip
	 */
	static void stormAttack()
	{
		Integer segmentIndex = Utils.genRandomNum(MainClass.nbSegments);
		Segment s = MainClass.segments.get(segmentIndex);
		double delay = s.getCost()*s.getZ();
		if(s.getCaravan()!=null)
		{
			System.out.println("Caravana de pe segmentul "+s.getId()+" a fost lovita de furtuna si intarziata cu "+delay);
			MainClass.remainingReserve-=delay;
		}
		else
		{
			System.out.println("Furtuna a lovit pe segmentul "+s.getId() +" care era gol.");
		}
	}

	/**
	 * Metoda care calculeaza intarziere posibila cauzata de o furtuna
	 */
	static double getStormImpact(ArrayList<ExpResult> scenario)
	{
		double stormProbability = Simulation.calculateStormProbability(scenario);
		if(MainClass.DEBUG_INTERPRET_DETAILS)
			System.out.println("Storm probability : "+stormProbability);
		
		double meanTimeOnRoad = Simulation.calculateMeanScenarioCost(scenario);
		if(MainClass.DEBUG_INTERPRET_DETAILS)
			System.out.println("Mean Cost : "+meanTimeOnRoad);
		
		double meanZ = Simulation.calculateMeanZ(scenario);
		if(MainClass.DEBUG_INTERPRET_DETAILS)
			System.out.println("Mean Z Delay : "+meanZ);
		
		double meantimeOnRoadIfStrom = meanTimeOnRoad+meanTimeOnRoad*meanZ;
		double predictedCostByStormOnly = meantimeOnRoadIfStrom*stormProbability + meanTimeOnRoad*(1-stormProbability);
		if(MainClass.DEBUG_INTERPRET)
			System.out.println("Cost(fct de probabilitate) daca ar lovi furtuna "+predictedCostByStormOnly + " dif: "+(predictedCostByStormOnly-meanTimeOnRoad));
		//ce impact va avea asupra lui y
		return predictedCostByStormOnly-meanTimeOnRoad;
	}

	/**
	 * Metoda care calculeaza Z-ul mediu al segmentelor pe care urmeaza sa intre caravanele in scenariul curent
	 */
	static double calculateMeanZ(ArrayList<ExpResult> scenario)
	{
		double totalZ=0;
		for(ExpResult move : scenario)
		{
			if(move.isIndiferent())
			{
				double tempMean = (MainClass.nodes.get(move.getNodeId()).getLeft().getZ() + MainClass.nodes.get(move.getNodeId()).getRight().getZ())/2;
				totalZ += tempMean;
			}
			else
				totalZ += MainClass.segments.get(move.getSegmentId()).getZ();
		}
		return totalZ/scenario.size();
	}

	static double calculateMeanScenarioCost(ArrayList<ExpResult> scenario)
	{
		double totalCost=0;
		for(ExpResult move : scenario)
		{
			if(move.isIndiferent())
				totalCost += MainClass.nodes.get(move.getNodeId()).getLeft().getCost();
			else
				totalCost += MainClass.segments.get(move.getSegmentId()).getCost();
		}
		return totalCost/scenario.size();
	}

	static double calculateStormProbability(ArrayList<ExpResult> scenario)
	{
		double nbActiveSegments = scenario.size();
		return nbActiveSegments/MainClass.nbSegments;
	}

	

	
}

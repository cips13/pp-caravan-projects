package profiling;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;


public class MainClass
{
	
	public static final boolean DEBUG_GENERATE=true;
	public static final boolean DEBUG_INTERPRET= true;
	public static final boolean DEBUG_INTERPRET_DETAILS= true;
	public static final boolean DEBUG_ANALIZE_SCENARIOS = false;
	
	//constantele clasei
	public static Integer NOMIN;
	public static Integer NOMAX;
	public static Double TMIN;
	public static Double X;
	public static Double Y;
	
	//informatii pentru reprezentarea lumii
	public static Integer nbNodes;
	public static Integer nbSegments;
	
	public static HashMap<Integer,Node> nodes= new HashMap<>();
	public static HashMap<Integer,Segment> segments = new HashMap<>();
	
	public static Integer startingNO;
	public static Integer endingNO = 0;
	
	public static Integer nbCaravans = 0; 
	public static Double remainingReserve;
	
	public static ArrayList<Integer> activeNodeIds = new ArrayList<>();
	public static ArrayList<Integer> activeSegmentIds = new ArrayList<>();
	
	private static ArrayList<ArrayList<ExpResult>> resultSet = new ArrayList<ArrayList<ExpResult>>();
	private static ArrayList<ScenarioAnRes> scenarioAnalisysResult = new ArrayList<>();
	
	private static boolean run = false;
	
	public static void main(String [] args) throws IOException
	{
		final long start = System.currentTimeMillis();
		
		BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
		
		BufferedReader input = new BufferedReader(new FileReader("buna200.in"));
		
		loadFile(input);
		
		readNodeInfo(input);
		readSegmentInfo(input);
		
		startingNO = (NOMIN+NOMAX)/2;
		Caravan startingCaravan = new Caravan(startingNO);
		nbCaravans++;
		nodes.get(0).setCaravan(startingCaravan);
		activeNodeIds.add(0);
		int step = 0;
		while(nbCaravans>0)
		{
			System.out.println("===================== STEP "+step+"=====================");
			System.out.println();
			System.out.println("NODURI: ");
			moveToRoad();
			System.out.println();
			System.out.println("Press Enter for segments........");
			//userInput.readLine();
			System.out.println("SEGMENTE: ");
			moveToHut();
			System.out.println();
			System.out.println("Press Enter for next step.......");
			//userInput.readLine();
			step++;
		}
		System.out.println(endingNO + " oameni au ajuns la final (" + (double)endingNO/(double)startingNO*100+"%) ");
		System.out.println("Duration "+ String.valueOf(System.currentTimeMillis()-start));
	}

	
	
	/**
	 * Metoda care muta caravanele din noduri pe drumuri. 
	 * Aici se iau deciziile referitoare la impartire.
	 */
	private static void moveToRoad()
	{
		generatePosibilties();
		int bestScenarioIndex = interpretResultSet();
		if(DEBUG_GENERATE)
			printGenerateResults();
		System.out.println("Scenariu cu index "+bestScenarioIndex+" a fost ales si va fi pus in executie.");
		ArrayList<ExpResult> scenario = resultSet.get(bestScenarioIndex);

		boolean skip = false;
		for(ExpResult move :scenario)
		{
			if(skip)
			{
				skip=false;
				continue; 
			}
			Node currentNode = nodes.get(move.getNodeId());
			Caravan currentCaravan = currentNode.getCaravan();
			Integer currentCaravanSize = currentCaravan.getSize();
			currentNode.setCaravan(null);
			//daca e indiferent trebuie sa vad daca o iau in stg sau dreapta
			if(move.isIndiferent())
			{	

				if(Utils.genRandomNum(2)==0)
				{
					currentNode.getLeft().setCaravan(currentCaravan);
					activeSegmentIds.add(currentNode.getLeft().getId());
					System.out.println("Caravana("+ currentCaravan.getSize() +")pers din nodul "+currentNode.getId()+" a cotit la stanga."+
										currentCaravan.getSize()+" oamneni pe segmentul "+ currentNode.getLeft().getId());
				}
				else
				{
					currentNode.getRight().setCaravan(currentCaravan);
					activeSegmentIds.add(currentNode.getRight().getId());
					System.out.println("Caravana ("+ currentCaravan.getSize() +")pers din nodul "+currentNode.getId()+" a cotit la dreapta."+
							currentCaravan.getSize()+" oamneni pe segmentul "+ currentNode.getRight().getId());
				}
			}
			else
			{
				
				if(move.isAllInOneDir())
				{
					Segment currentSegment = segments.get(move.getSegmentId());
					currentSegment.setCaravan(currentCaravan);
					activeSegmentIds.add(move.getSegmentId());
					System.out.println("Caravana ("+ currentCaravan.getSize() +")pers din nodul "+currentNode.getId()+" a cotit."+
									currentCaravan.getSize()+" oamneni pe segmentul "+ currentSegment.getId());
				}
				else
				{
					
					skip=true;
					Integer nbLeft = currentCaravanSize/2;
					Integer nbRight = currentCaravanSize-nbLeft;
					
					currentNode.getLeft().setCaravan(new Caravan(nbLeft));
					activeSegmentIds.add(currentNode.getLeft().getId());
					
					currentNode.getRight().setCaravan(new Caravan(nbRight));
					activeSegmentIds.add(currentNode.getRight().getId());
					
					currentCaravan=null;
					//incrementez doar cu 1 pt ca am distrus 1 si am creat 2 
					nbCaravans+=1;
					
					System.out.print("Caravana din nodul "+currentNode.getId()+"s-a impartit.");
					System.out.print(nbLeft+" oameni au cotit la stanga pe drumul "+ currentNode.getLeft().getId());
					System.out.println(" iar "+nbRight+" oameni au cotit la dreapta pe drumul "+currentNode.getRight().getId());
					
				}
				
			}
		}
		activeNodeIds.clear();
		resultSet.clear();
		scenarioAnalisysResult.clear();
	}

	/**
	 * Metoda care muta caravanele de pe drumuri in nodurile urmatoare
	 * Aici se adauga oameni, se simuleaza atacul hotilor , sau furtunile de nisip 
	 */
	private static void moveToHut()
	{
		for(Iterator<Integer> i = activeSegmentIds.iterator();i.hasNext();)
		{
			Integer segmentId = i.next();
			if(Simulation.attackByThieves(segmentId))
	    		i.remove();
			
			Simulation.addPeople(segmentId);
		}
		Simulation.stormAttack();
    	Simulation.argue();
		System.out.println("Remaining Y "+remainingReserve);
		if(remainingReserve<0)
		{
			System.out.println("GAME OVER!");
			nbCaravans=-1;
			return;
		}
		//aici argue si add people si atack by thieves si mai jos doar mutarea
		for(Iterator<Integer> i = activeSegmentIds.iterator();i.hasNext();)
		{
			Integer segmentId = i.next();
			i.remove();
		    Segment currentSegment = segments.get(segmentId);

		    Node inNode = currentSegment.getIn();
	    	Node outNode = currentSegment.getOut();
	    	Caravan currentCaravan = currentSegment.getCaravan();
	    	currentSegment.setCaravan(null);
	    	
		    //daca capatul segmentului este oaza B
		    if(currentSegment.getOut()==null)
		    {
		    	endingNO+=currentCaravan.getSize();
		    	System.out.println(currentCaravan.getSize()+
		    					   " oameni au ajuns la oaza B de pe segmentul "+currentSegment.getId());
		    	nbCaravans--;
		    }
		    else
		    {
		    	outNode.setCaravan(currentCaravan);
		    	activeNodeIds.add(outNode.getId());
		    	System.out.println("Caravana ("+ currentCaravan.getSize() +")pers plecata din nodul "+inNode.getId() + " a luat-o pe segmentul "+segmentId +" si a ajuns in nodul "+outNode.getId());
		    	
		    }
		    
		}
	}
	private static int interpretResultSet()
	{
		//int bestIndex=-1;
		scenarioAnalisysResult.clear();
		for(int i=0;i<resultSet.size();i++)
		{
			ArrayList<ExpResult> scenario = resultSet.get(i);
			double yCost=0;
			yCost += Simulation.getStormImpact(scenario);
			yCost += Utils.getPeopleImpact(scenario);
			if(DEBUG_INTERPRET)
				System.out.println("Y cost pe mutare ar fi "+yCost);
			int nonSplitNb = Utils.getNbOfNonSplits(scenario);
			int surplus = Utils.getNoPeopleAboveNomax(scenario);
			int subNomin = Utils.getNoCaravansBelowNomin(scenario);
			scenarioAnalisysResult.add(new ScenarioAnRes(i,nonSplitNb,remainingReserve-yCost,surplus,subNomin));
		}
		System.out.println("Remaining Y "+remainingReserve);
		return analizeScenarios();
		
	}

	private static int analizeScenarios()
	{
		Collections.sort(scenarioAnalisysResult);
		ScenarioAnRes first = scenarioAnalisysResult.get(0);
		if(DEBUG_ANALIZE_SCENARIOS)
			System.out.println(scenarioAnalisysResult);
		if(first.getRemainingY()<0)
			run=true;
		return first.getScenarioId();
	}

	private static void generatePosibilties()
	{
		resultSet.clear();
		genResultSet(activeNodeIds,new ArrayList<ExpResult>());
	}

	/**
	 * Metoda care genereaza toate posibilitatile de mutari.
	 */
	private static void genResultSet(ArrayList<Integer> nodeIds,
			ArrayList<ExpResult> rezIntermediare)
	{
		//daca s-a ajuns la sf
		if(nodeIds.isEmpty())
		{
			resultSet.add(rezIntermediare);
			return;
		}
		
		Integer nodeId = nodeIds.remove(0);
		Node currentNode = nodes.get(nodeId);
		System.out.println("NODE ID "+nodeId +"CAR "+currentNode.getCaravan());
		Integer leftId = currentNode.getLeft().getId();
		Integer rightId = currentNode.getRight().getId();
		Segment leftSegment = segments.get(leftId);
		Segment rightSegment = segments.get(rightId);
	//	System.out.println("Nod "+nodeId);
		//daca segmentele au costuri egale trebuie sa expandez doar 2 cazuri
		if(leftSegment.getCost().doubleValue()==rightSegment.getCost().doubleValue())
		{
			ArrayList<ExpResult> rezInt1 = Utils.copyArray(rezIntermediare);
			ArrayList<ExpResult> rezInt2 = Utils.copyArray(rezIntermediare);
			
			//se duc toti pe una din cai
			ExpResult case1 = new ExpResult(null,currentNode.getCaravan().getSize(),true,nodeId,true);
			rezInt1.add(case1);
			ArrayList<Integer> nodeIdsCopy1 = Utils.copyArray(nodeIds);
			genResultSet(nodeIdsCopy1, rezInt1);
			
			//se impart pe jumatate
			Integer leftSize = currentNode.getCaravan().getSize()/2;
			Integer rightSize = currentNode.getCaravan().getSize()-leftSize;
			ExpResult case2a = new ExpResult(leftId, leftSize,false,nodeId,false);
			ExpResult case2b = new ExpResult(rightId, rightSize, false, nodeId,false);
			rezInt2.add(case2a); rezInt2.add(case2b);
			ArrayList<Integer> nodeIdsCopy2 = Utils.copyArray(nodeIds);
			genResultSet(nodeIdsCopy2, rezInt2);
		}
		//daca costurile sunt diferite am: totiStanga,totiDreapta,sau jumatate jumatate
		else
		{
			ArrayList<ExpResult> rezInt1 = Utils.copyArray(rezIntermediare);
			ArrayList<ExpResult> rezInt2 = Utils.copyArray(rezIntermediare);
			ArrayList<ExpResult> rezInt3 = Utils.copyArray(rezIntermediare);
			
			//se duc toti spre stanga
			ExpResult case1 = new ExpResult(leftId,currentNode.getCaravan().getSize(),false,nodeId,true);
			rezInt1.add(case1);
			ArrayList<Integer> nodeIdsCopy1 = Utils.copyArray(nodeIds);
			genResultSet(nodeIdsCopy1, rezInt1);
			
			//se duc toti spre dreapta
			ExpResult case2 = new ExpResult(rightId,currentNode.getCaravan().getSize(),false,nodeId,true);
			rezInt2.add(case2);
			ArrayList<Integer> nodeIdsCopy2 = Utils.copyArray(nodeIds);
			genResultSet(nodeIdsCopy2, rezInt2);
			
			//se impart
			Integer leftSize = currentNode.getCaravan().getSize()/2;
			Integer rightSize = currentNode.getCaravan().getSize()-leftSize;
			ExpResult case3a = new ExpResult(leftId, leftSize,false,nodeId,false);
			ExpResult case3b = new ExpResult(rightId, rightSize, false, nodeId,false);
			rezInt3.add(case3a); rezInt3.add(case3b);
			ArrayList<Integer> nodeIdsCopy3 = Utils.copyArray(nodeIds);
			genResultSet(nodeIdsCopy3, rezInt3);
		}
	}
	/**
	 * Metoda de debugging care afiseaza rezultatele generate
	 */
	private static void printGenerateResults()
	{
		if(DEBUG_GENERATE)
		{
			for(int i =0;i<resultSet.size();i++)
			{
				ArrayList<ExpResult> currentCourse = resultSet.get(i);
				System.out.println("----------CAZ "+i+"----------");
				System.out.println();
				System.out.println(currentCourse);
				System.out.println();
			}
		}
	}

	/**
	 * Metoda citeste nodurile din fisier si creaza asocierea nod - segment stang si drept.
	 * De asemenea toate nodurile si segmentele sunt introduse in nodes si segments cu id-ul lor drept cheie
	 */
	private static void readNodeInfo(BufferedReader input) throws IOException
	{
		for(int i=0;i<nbNodes;i++)
		{
			String line = input.readLine();
			String [] info = line.split(" ");
			
			Integer nodeId = Integer.parseInt(info[0]);
			Integer segm1Id = Integer.parseInt(info[1]);
			Integer segm2Id = Integer.parseInt(info[2]);
			
			Segment segm1 = new Segment(segm1Id);
			Segment segm2 = new Segment(segm2Id);
			Node n= new Node(nodeId,segm1,segm2);
			
			nodes.put(nodeId, n);
			segments.put(segm1Id, segm1);
			segments.put(segm2Id,segm2);
			
		}
	}
	/**
	 * Se citesc informatiile despre segmente si se actualizeaza referintele nodurilor asociate acestora.
	 * Daca nodul de out are id -1 inseamna ca segmentul este direct legat de B, caz in care nodul are valoarea null
	 */
	private static void readSegmentInfo(BufferedReader input)
			throws IOException
	{
		for(int i=0;i<nbSegments;i++)
		{
			String line = input.readLine();
			String [] info = line.split(" ");
			
			Integer segmentId = Integer.parseInt(info[0]);
			Integer inId = Integer.parseInt(info[1]);
			Integer outId = Integer.parseInt(info[2]);
			Double cost = Double.parseDouble(info[3]);
			Double z = Double.parseDouble(info[4]);
			Node in = nodes.get(inId);
			Node out = outId == -1 ? null : nodes.get(outId);
			
			Segment segment = segments.get(segmentId);
			segment.setIn(in);
			segment.setOut(out);
			segment.setCost(cost);
			segment.setZ(z);
			
		}
	}
	/**
	 * Metoda care incarca variabilele de sistem din fisier
	 */
	private static void loadFile(BufferedReader input) throws IOException
	{
		NOMIN = Integer.parseInt(input.readLine());
		NOMAX = Integer.parseInt(input.readLine());
		TMIN = Double.parseDouble(input.readLine());
		X = Double.parseDouble(input.readLine());
		Y = Double.parseDouble(input.readLine());
		remainingReserve = Y;
		
		nbNodes = Integer.parseInt(input.readLine());
		nbSegments = Integer.parseInt(input.readLine());
	}
}

package profiling;

public class Node 
{
	private Integer id;
	private Segment left;
	private Segment right;
	private Caravan caravan;
	
	public Node(Integer id,Segment left,Segment right)
	{
		this.setId(id);
		this.setLeft(left);
		this.setRight(right);
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Segment getLeft()
	{
		return left;
	}

	public void setLeft(Segment left)
	{
		this.left = left;
	}

	public Segment getRight()
	{
		return right;
	}

	public void setRight(Segment right)
	{
		this.right = right;
	}

	public Caravan getCaravan()
	{
		return caravan;
	}

	public void setCaravan(Caravan caravan)
	{
		this.caravan = caravan;
	}
	

}

package profiling;
import java.util.ArrayList;
import java.util.Random;


public class Utils
{
	public static ArrayList copyArray(ArrayList array)
	{
		ArrayList result = new ArrayList();
		for(int i=0; i<array.size();i++)
		{
			result.add(array.get(i));
		}
		return result;
	}
	public static Integer genRandomNum(Integer maxValue)
	{
		Random rand = new Random();
		return rand.nextInt(maxValue);
	}
	/**
	 * Metoda care intoarce nr de decizii in care caravana nu se desparte din succesiunea curenta de mutari
	 */
	static int getNbOfNonSplits(ArrayList<ExpResult> scenario)
	{
		int nbNonSplits =0;
		for(ExpResult move:scenario)
			if(move.isAllInOneDir())
				nbNonSplits++;
		return nbNonSplits;
	}
	/**
	 * Metoda care calculeaza ce intarziere ar putea introduce oamenii peste NOMAX din scenariul curent
	 */
	static double getPeopleImpact(ArrayList<ExpResult> scenario)
	{
		double yCost=0;
		for(ExpResult move : scenario)
		{
			Double nbAfterAdd = 1.5*move.getNbOfPeople();
			if(nbAfterAdd.intValue()>MainClass.NOMAX)
			{
				Segment s;
				if(move.isIndiferent())
					s = MainClass.nodes.get(move.getNodeId()).getLeft();
				else
					s = MainClass.segments.get(move.getSegmentId());
				
				int difference = nbAfterAdd.intValue()-MainClass.NOMAX;
				yCost += MainClass.X*s.getCost() * difference;
			}
		}
		if(MainClass.DEBUG_INTERPRET)
			System.out.println("Cost (fct de oameni adaugati): " +yCost);
		return yCost;
	}
	/**
	 * Metoda care intoarce nr total de oameni peste NOMAX din scenariul curent
	 */
	static int getNoPeopleAboveNomax(ArrayList<ExpResult> scenario) 
	{
		int difference=0;
		for(ExpResult move : scenario)
		{
			Double nbAfterAdd = 1.5*move.getNbOfPeople();
			if(nbAfterAdd.intValue()>MainClass.NOMAX)
				difference += nbAfterAdd.intValue()-MainClass.NOMAX;
		}
		if(MainClass.DEBUG_INTERPRET)
			System.out.println(difference+" oameni peste NOMAX.");
		return difference;
	}
	/**
	 * Metoda care intoarce nr de caravane sub NOMIN din scenariul curent
	 */
	static int getNoCaravansBelowNomin(ArrayList<ExpResult> scenario) 
	{
		int result = 0;
		for(ExpResult move : scenario)
		{
			if(move.getNbOfPeople()<MainClass.NOMIN)
				result++;
		}
		if(MainClass.DEBUG_INTERPRET)
			System.out.println(result+" caravane sub NOMIN.");
		return result;
	}
}

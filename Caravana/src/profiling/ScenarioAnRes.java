package profiling;
import java.util.Comparator;


public class ScenarioAnRes implements Comparable
{

	int scenarioId;
	int nbOfNonSplits;
	double remainingY;
	private int surplus;
	private int nbBelowNOMIN;
	
	public ScenarioAnRes(int scenarioId, int nbOfNonSplits, double remainingY,int surplus,int nbBelowNOMIN)
	{
		this.scenarioId=scenarioId;
		this.nbOfNonSplits = nbOfNonSplits;
		this.remainingY = remainingY;
		this.surplus= surplus;
		this.nbBelowNOMIN = nbBelowNOMIN;
	}
	@Override 
	public String toString()
	{
		return "ScenID "+scenarioId +" remY "+ remainingY + " nbOfNonSplits " +nbOfNonSplits +" surplus "+surplus +" nbCar below NOMIN "+nbBelowNOMIN;
	}
	@Override
	public int compareTo(Object obj0)
	{
		ScenarioAnRes scenario1 = this;
		ScenarioAnRes scenario2 = (ScenarioAnRes)obj0;
		
		if(scenario1.getRemainingY()>=0 && scenario2.getRemainingY()>=0)
		{
			//daca au acelasi surplus, aleg varianta in care oamenii raman uniti
			if(scenario1.getSurplus()==scenario2.getSurplus())
			{
				return scenario2.getNbOfNonSplits()-scenario1.getNbOfNonSplits();
			}
			//daca scenario 1 are mai multi oameni peste NOMAX
			else 
				return scenario2.getSurplus()-scenario1.getSurplus();
		}
		else if(scenario1.getRemainingY()<0 && scenario2.getRemainingY()>=0)
		{
			return 1;
		}
		else if(scenario1.getRemainingY()>=0 && scenario2.getRemainingY()<0)
			return -1;
		else if(scenario1.getRemainingY()<0 && scenario2.getRemainingY()<0)
		{
			if(scenario1.getNbBelowNOMIN()==scenario2.getNbBelowNOMIN())
				return scenario1.getNbOfNonSplits() - scenario2.getNbOfNonSplits();
			else
				return scenario1.getNbBelowNOMIN()-scenario2.getNbBelowNOMIN();
		}
		
		System.out.println("!!!!!!!!!!Ceva ciudat la comparare " + scenario1.getRemainingY() +"  "+scenario2.getRemainingY());
		return 0;
	}

	
	public int getScenarioId()
	{
		return scenarioId;
	}
	public int getNbOfNonSplits()
	{
		return nbOfNonSplits;
	}
	public double getRemainingY()
	{
		return remainingY;
	}
	public void setScenarioId(int scenarioId)
	{
		this.scenarioId = scenarioId;
	}
	public void setNbOfNonSplits(int nbOfNonSplits)
	{
		this.nbOfNonSplits = nbOfNonSplits;
	}
	public void setRemainingY(double remainingY)
	{
		this.remainingY = remainingY;
	}
	public int getSurplus()
	{
		return surplus;
	}
	public void setSurplus(int surplus)
	{
		this.surplus = surplus;
	}
	public int getNbBelowNOMIN()
	{
		return nbBelowNOMIN;
	}
	public void setNbBelowNOMIN(int nbBelowNOMIN)
	{
		this.nbBelowNOMIN = nbBelowNOMIN;
	}

	

	

}
